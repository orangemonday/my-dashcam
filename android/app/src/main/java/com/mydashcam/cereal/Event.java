package com.mydashcam.cereal;

import android.os.SystemClock;
import org.capnproto.MessageBuilder;

public class Event {

    public MessageBuilder message;
    public Logger.Event.Builder builder;

    public Event () {
        message = new MessageBuilder();
        builder = message.initRoot(Logger.Event.factory);

        builder.setLogMonoTime(SystemClock.elapsedRealtimeNanos());
    }
}
