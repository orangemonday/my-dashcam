package com.mydashcam.camera;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

public class LocationService {
    interface Callback {
        void onLocationChanged(Location location);
        void onStatusChanged(String provider, int status, Bundle extras);
    }

    private Callback mCallback;

    private LocationManager mLocationManager;

    private final LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            mCallback.onLocationChanged(location);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            mCallback.onStatusChanged(provider, status, extras);
        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    LocationService(Context context) {
        mLocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
    }

    public void setCallback(Callback callback) { mCallback = callback; }

    @SuppressLint("MissingPermission")
    public void start() {
        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                1000, 0, mLocationListener);
    }

    public void stop() {
        mLocationManager.removeUpdates(mLocationListener);
    }
}
