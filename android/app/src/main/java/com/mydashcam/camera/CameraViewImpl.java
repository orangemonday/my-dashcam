package com.mydashcam.camera;

import android.view.View;

public abstract class CameraViewImpl {

    // TODO: Refactor this shit

    protected Callback mCallback;

    abstract public boolean start();
    abstract public void stop();
    abstract public boolean startRecording();
    abstract public void stopRecording();
    abstract public View getView();

    interface Callback {
        void onRecording(int secs, int mins);
        void onLocationUpdate(float distanceTravel, float speed);
    }
}
