package com.mydashcam;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.HandlerThread;

import com.mydashcam.camera.DataWriter;
import com.mydashcam.cereal.Event;
import com.mydashcam.cereal.Logger;

import java.util.ArrayList;
import java.util.Iterator;

// TODO: NMEA GPS, WIFI, TELEPHONY
public class MDSensorLogger {
    private final Context mContext;
    private final DataWriter mDataWriter;

    private final Handler mHandler;
    private final HandlerThread mHandlerThread;

    private ArrayList<SensorLogger> mSensorLoggerList;
    private final int[] sensorTypes = {
            Sensor.TYPE_ACCELEROMETER,
            Sensor.TYPE_GYROSCOPE,
            Sensor.TYPE_GYROSCOPE_UNCALIBRATED,
            Sensor.TYPE_MAGNETIC_FIELD,
            Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED,
            Sensor.TYPE_PRESSURE
    };

    private boolean isLogging = false;

    public MDSensorLogger(Context context, DataWriter dataWriter) {
        mContext = context;
        mDataWriter = dataWriter;
        mSensorLoggerList = new ArrayList();

        mHandlerThread = new HandlerThread("sensor_handler_thread");
        mHandlerThread.start();
        mHandler = new Handler(mHandlerThread.getLooper());

        for (int i = 0; i < sensorTypes.length; i++) {
            SensorLogger sensorLogger = new SensorLogger(context, sensorTypes[i], mDataWriter, mHandler);
            mSensorLoggerList.add(sensorLogger);
        }
    }

    public void startLoggin() {
        if (isLogging) {
            return;
        }

        for (Iterator<SensorLogger> it = mSensorLoggerList.iterator(); it.hasNext(); ) {
            SensorLogger s = it.next();
            s.register();
        }

        isLogging = true;
    }

    public void stopLogging() {
        if (!isLogging) {
            return;
        }

        for (Iterator<SensorLogger> it = mSensorLoggerList.iterator(); it.hasNext(); ) {
            SensorLogger s = it.next();
            s.unregister();
        }

        isLogging = false;
    }

    public class SensorLogger implements SensorEventListener {
        private final SensorManager mManager;
        private final Sensor mSensor;
        private final DataWriter mDataWriter;
        private final Handler mHandler;

        public SensorLogger(Context context, int sensorType, DataWriter dataWriter, Handler handler) {
            mManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
            mSensor = mManager.getDefaultSensor(sensorType);
            mDataWriter = dataWriter;
            mHandler = handler;
        }

        public void register() {
            mManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_NORMAL, mHandler);
        }

        public void unregister() {
            mManager.unregisterListener(this, mSensor);
        }

        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {
            Event e = new Event();
            Logger.SensorEventData.Builder sensorEventData = e.builder.initSensorEventData();

            sensorEventData.setVersion(sensorEvent.sensor.getVersion());
            sensorEventData.setType(sensorEvent.sensor.getType());
            sensorEventData.setTimestamp(sensorEvent.timestamp);
            sensorEventData.setSource(Logger.SensorEventData.SensorSource.ANDROID);

            Logger.SensorEventData.SensorVec.Builder sensorVec = null;
            int type = sensorEvent.sensor.getType();
            switch (type) {
                case Sensor.TYPE_ACCELEROMETER:
                    sensorVec = sensorEventData.initAcceleration();
                    break;
                case Sensor.TYPE_GYROSCOPE:
                    sensorVec = sensorEventData.initGyro();
                    break;
                case Sensor.TYPE_GYROSCOPE_UNCALIBRATED:
                    sensorVec = sensorEventData.initGyroUncalibrated();
                    break;
                case Sensor.TYPE_MAGNETIC_FIELD:
                    sensorVec = sensorEventData.initMagnetic();
                    break;
                case Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED:
                    sensorVec = sensorEventData.initMagneticUncalibrated();
                    break;
                case Sensor.TYPE_PRESSURE:
                    sensorVec = sensorEventData.initPressure();
                    break;
            }

            if (sensorVec != null) {
                int length = sensorEvent.values.length;
                sensorVec.initV(length);
                for (int i = 0; i < length; i++) {
                    sensorVec.getV().set(i, sensorEvent.values[i]);
                }
                sensorVec.setAccuracy((byte) sensorEvent.accuracy);
                mDataWriter.writeEvent(e);
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int i) {

        }
    }
}
