package com.mydashcam;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPOutputStream;

import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class UploaderService extends Service implements Runnable {
    private final String TAG = "UploaderService";
    private OkHttpClient mHttpClient = new OkHttpClient();
    private Thread mThread;

    private boolean mIsStarted = false;

    private final Integer ONGOING_NOTIFICATION_ID = 1;

    private void startUploading(List<File> folderList) {
        int numOfFolderToUpload = folderList.size();
        int uploaded = 0;

        // Loop through each directory
        for (File folder: folderList) {
            File[] files = folder.listFiles();

            // Loop through each file (acamera & log)
            for (File f: files) {
                File uploadFile = f;

                // prepare log file
                if (f.getName().equals("log")) {
                    uploadFile = prepareLogFile(f);
                }

                //TODO: Report uploading what, and how much uploaded
                do {
                    int statusCode;
                    statusCode = upload(uploadFile);

                    // Retry uploading until status code 200
                    if (statusCode == 200) {
                        // Delete file after uploading
                        Log.d(TAG, "Uploaded " + uploadFile.getPath());
                        uploadFile.delete();
                        break;
                    } else {
                        Log.e(TAG, "statusCode not 200, restart upload");
                        if (!isInternetAvailable()) {
                            Log.d(TAG, "No internet available to upload");
                            stopSelf();
                            return;
                        }
                    }
                } while (true);
            }
            uploaded++;
        }
    }

    private boolean isInternetAvailable() {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) this.getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        if (activeNetworkInfo == null) {
            return false;
        }

        if (!activeNetworkInfo.isConnectedOrConnecting()) {
            return false;
        }

        if (activeNetworkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
            if (!connectivityManager.isActiveNetworkMetered()) {
                return true;
            }
        }

        return false;
    }

    private int upload(File file) {
        JSONObject info = requestUploadInfo(
                "v1.0",
                "me/upload_url/",
                file.getPath());
        if (info == null) {
            return -1;
        }

        try {
            String url = info.getString("url");

            JSONObject headerJson = info.getJSONObject("headers");
            Map<String, String> headersMap = new LinkedHashMap();
            for (Iterator<String> it = headerJson.keys(); it.hasNext(); ) {
                String k = it.next();
                headersMap.put(k, headerJson.getString(k));
            }
            Headers h = Headers.of(headersMap);

            Request r = new Request.Builder()
                    .url(url)
                    .put(RequestBody.create(MediaType.parse("application/octet-stream"), file))
                    .headers(h)
                    .build();

            return mHttpClient.newCall(r).execute().code();
        } catch (JSONException e) {
            Log.e(TAG, "Failed to access json fields");
        } catch (IOException e) {
            Log.e(TAG, "Failed to upload file");
        }

        return -1;
    }

    private JSONObject requestUploadInfo(String version, String endpoint, String path) {
        String url = "http://192.168.0.199:5000/api/";
        String accessToken = MDPreferences.getIdToken(this);

        Uri.Builder urlBuilder = Uri.parse(url).buildUpon();
        urlBuilder.appendEncodedPath(version);
        urlBuilder.appendEncodedPath(endpoint);
        urlBuilder.appendQueryParameter("path", path);

        Request request = new Request.Builder()
                .url(urlBuilder.build().toString())
                //.header("User-Agent", "mydasmcam-android-0.1")
                .header("Authorization", "JWT " + accessToken)
                //.header("x-mydashcam-android-version", "0.1")
                //.header("x-mydashcam-android-model", Build.MODEL)
                .header("x-mydashcam-android-serial", Build.SERIAL)
                //.header("x-mydashcam-android-sdk", String.valueOf(Build.VERSION.SDK_INT))
                .build();

        try {
            Response response = mHttpClient.newCall(request).execute();
            return new JSONObject(response.body().string());
        } catch (IOException e) {
            Log.e(TAG, "Failed to request for upload url");
        } catch (JSONException e) {
            Log.e(TAG, "Failed to decode json response");
        }

        return null;
    }

    private File prepareLogFile(File logFile) {
        if (!logFile.getName().equals("log")) {
            Log.e(TAG, "Error: logFile is not a log file");
            return null;
        }

        if (logFile.length() == 0) {
            Log.e(TAG, "logFile is empty");
            return null;
        }

        try {
            Log.d(TAG, "Compressing " + logFile.getPath());

            // prepare input stream
            FileInputStream inputStream = new FileInputStream(logFile);

            // prepare output stream
            String filename = String.format("%s.gz", logFile.getPath());
            File compressedFile = new File(filename);
            FileOutputStream outputStream = new FileOutputStream(compressedFile);
            GZIPOutputStream gzipOutputStream = new GZIPOutputStream(outputStream);

            // compress input file to output file
            byte[] buffer = new byte[1024];
            int bytesRead;
            while((bytesRead = inputStream.read(buffer)) > 0) {
                gzipOutputStream.write(buffer, 0, bytesRead);
            }
            Log.d(TAG, "Finished compressing");

            // close both input and output stream
            gzipOutputStream.close();
            inputStream.close();

            // delete logFile when done
            logFile.delete();

            return compressedFile;
        } catch (FileNotFoundException e) {
            Log.e(TAG, "logFile does not exist");
        } catch (IOException e) {
            Log.e(TAG, "Cannot create gzipOutputStream");
        }

        return null;
    }

    private List<File> getFoldersReadyToUpload() {
        final List<File> dataDirs = MDDataDirectory.getAllDataDirs(this);
        List<File> folderList = new ArrayList<>();

        for (File dataDir: dataDirs) {
            File[] dirs = dataDir.listFiles();
            if (dirs.length == 0) {
                continue;
            }

            for (File d: dirs) {
                FilenameFilter filter = new FilenameFilter() {
                    @Override
                    public boolean accept(File dir, String filename) {
                        if (filename.equals("acamera") || filename.equals("log.gz")) {
                            return true;
                        }
                        return false;
                    }
                };

                File[] files = d.listFiles(filter);
                if (files.length == 0) {
                    continue;
                }

                folderList.add(d);
            }
        }

        if (folderList.isEmpty()) {
            return null;
        }
        return folderList;
    }

    @Override
    public void run() {
//        if (!isInternetAvailable()) {
//            Log.d(TAG, "No internet available to upload");
//            stopSelf();
//            return;
//        }

        // check if there is any file tox upload
        List<File> readyFolders = getFoldersReadyToUpload();
        if (readyFolders == null) {
            Log.d(TAG, "Nothing to upload");
            stopSelf();
            return;
        }

        startUploading(readyFolders);

        stopSelf();
    }

    public void commandStart() {
        Log.d(TAG, "commandStart");
        if (!mIsStarted) {
            moveToStartedState();
            return;
        }

        if (mThread == null) {
            mThread = new Thread(this);
            mThread.start();
        }

        createNotification(this);
    }

    private void commandStop() {
        stopSelf();
    }

    private void moveToStartedState() {
        Intent intent = createIntent(this, Command.START);
        if (Build.VERSION.SDK_INT >= 26) {
            startForegroundService(intent);
        } else {
            startService(intent);
        }
    }

    private void createNotification(Service context) {

        PendingIntent piStopService =
                PendingIntent.getService(
                        context,
                        0,
                        createIntent(context, Command.STOP),
                        0);

        NotificationCompat.Action stopAction =
                new NotificationCompat.Action.Builder(
                        0,
                        "Stop Uploading",
                        piStopService)
                        .build();

        PendingIntent piLaunchMainActivity =
                PendingIntent.getService(
                        context,
                        0,
                        createIntent(context, Command.DEFAULT),
                        0);

        int pendingUpload = MDDataDirectory.getPendingUpload(context);
        Notification mNotification =
                new NotificationCompat.Builder(context)
                        //TODO Change Small Icon & Add Big Icon
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setLargeIcon(BitmapFactory.decodeResource(
                                context.getResources(), R.mipmap.ic_launcher))
                        .setContentTitle("Uploading")
                        .setContentText("0/" + pendingUpload)
                        .setProgress(pendingUpload, 0, false)
                        .setContentIntent(piLaunchMainActivity)
                        .addAction(stopAction)
                        .setOngoing(true)
                        .build();

        context.startForeground(ONGOING_NOTIFICATION_ID, mNotification);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");

        mIsStarted = true;

        int cmdInt = intent.getIntExtra(Command.intentID, Command.DEFAULT.ordinal());
        Command cmd = Command.values()[cmdInt];
        processCommand(cmd);

        return START_NOT_STICKY;
    }

    private void processCommand(Command cmd) {
        if (cmd == Command.START) {
            commandStart();
        }
        else if (cmd == Command.STOP) {
            commandStop();
        }
    }

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate");
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public static Intent createIntent(@NonNull Context context, Command cmd) {
        Intent intent = new Intent(context, UploaderService.class);
        intent.putExtra(Command.intentID, cmd.ordinal());
        return intent;
    }

    enum Command {
        START, STOP, DEFAULT;

        public final static String intentID = "command";
    };
}
