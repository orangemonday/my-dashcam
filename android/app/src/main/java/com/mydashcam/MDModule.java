package com.mydashcam;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.uimanager.NativeViewHierarchyManager;
import com.facebook.react.uimanager.UIBlock;
import com.facebook.react.uimanager.UIManagerModule;

public class MDModule extends ReactContextBaseJavaModule {

    private Context mContext;

    public MDModule(ReactApplicationContext context) {
        super(context);
        mContext = context;
    }

    @Override
    public String getName() {
        return "MDModule";
    }

    @ReactMethod
    public void startUploaderService() {
        final ReactApplicationContext context = getReactApplicationContext();

        Intent intent = UploaderService.createIntent(context, UploaderService.Command.START);

        if (Build.VERSION.SDK_INT >= 26) {
            context.startForegroundService(intent);
        } else {
            context.startService(intent);
        }
    }

    @ReactMethod
    public void isLoggedIn(final Promise promise) {
        if (MDPreferences.isLoggedIn(mContext)) {
            promise.resolve(null);
        } else {
            promise.reject("Not logged in", "There is no user in shared Preferences");
        }
    }

    @ReactMethod
    public void setUser(final String username, final String email, final String idToken) {
        MDPreferences.setUser(mContext, username, email, idToken);
    }

    @ReactMethod
    public void getUser(final Promise promise) {
        WritableMap map = Arguments.createMap();

        map.putString("username", MDPreferences.getUsername(mContext));
        map.putString("email", MDPreferences.getEmail(mContext));
        map.putString("idToken", MDPreferences.getIdToken(mContext));

        promise.resolve(map);
    }

    @ReactMethod
    public void removeUser() {
        MDPreferences.removeUser(mContext);
    }
}
