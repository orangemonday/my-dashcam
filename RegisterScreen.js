import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  TouchableHighlight,
} from "react-native";

import Config from './config.js'

export default class TOSScreen extends Component {
  static navigationOptions = {
    header: null,
  }
  
  constructor(props) {
    super(props)
    this.state = {
      accountName: '',
      valError: false,
      valErrorMessage: '',
      submitError: false,
    }
  }

  _inputError = (message) => {
    this.setState({
      valError: true,
      valErrorMessage: message
    })
  }

  _validate = () => {
    const accountName = this.state.accountName
    var re = /^[a-zA-Z0-9_]+$/;
    if (accountName === '') {
      this._inputError('Account name must not be empty')
      return false
    }

    if (accountName.length < 2 || accountName.length > 32) {
      this._inputError('Account name must be between 2 and 32 characters long')
      return false
    }

    if (!re.test(accountName)) {
      this._inputError('Account name must only contain letters, numbers and underscores')
      return false
    }

    this.setState({valError: false})
    return true
  }

  _submit = async () => {
    if (this._validate()) {
      try {
        const { navigation } = this.props;
        const user = navigation.getParam('user', null);
        let response = await fetch(Config.API_URL + "/auth/register/", {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            id_token: user.idToken,
            name: this.state.accountName,
          }),
        });
        const json = await response.json()

        if (response.status == 400) {
          throw json.message;
        }

        this.props.navigation.navigate("TOS", {json});
      } catch (error) {
        console.warn(error)
        if (error === 'Username is used') {
          this._inputError('Account name is used')
        } else {
          this.setState({submitError: true});
        }
      }
    }
  }

  _renderError = () => {
    if (this.state.valError) {
      return (
        <Text style={styles.errorText}>{this.state.valErrorMessage}</Text>
      )
    }
  }

  _renderSubmitError = () => {
    if (this.state.submitError) {
      return (
        <View style={styles.error}>
          <Text>There was a problem registering.</Text>
          <Text>Please try again.</Text>
        </View>
      )
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.label}>Register</Text>
        <TextInput 
          style={styles.input}
          placeholder="Account Name"
          onChangeText={(accountName) => this.setState({accountName})}
        />
        
        {this._renderError()}

        <TouchableHighlight
          style={styles.button}
          onPress={this._submit}>
          <Text>Submit</Text>
        </TouchableHighlight>

        {this._renderSubmitError()}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
  },
  label: {
    padding: 4,
    fontWeight: 'bold',
    fontSize: 24,
  },
  input: {
    height: 60,
  },
  errorText: {
    padding: 4,
    color: 'red',
  },
  button: {
    marginTop: 16,
    padding: 16,
    borderRadius: 16,
    borderWidth: 1,
    alignItems: 'center',
  },
  error: {
    marginTop: 8,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

