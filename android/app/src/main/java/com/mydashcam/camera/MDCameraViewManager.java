package com.mydashcam.camera;

import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.common.MapBuilder;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.ViewGroupManager;

import java.util.Map;

import javax.annotation.Nullable;

public class MDCameraViewManager extends ViewGroupManager<MDCameraView> {

    private static final String REACT_CLASS = "MDCamera";

    @Override
    public String getName() {
        return REACT_CLASS;
    }

    @Override
    protected MDCameraView createViewInstance(ThemedReactContext themedReactContext) {
        return new MDCameraView(themedReactContext);
    }

    @Override
    public void onDropViewInstance(MDCameraView view) {
        view.stop();
        super.onDropViewInstance(view);
    }

    @Nullable
    @Override
    public Map getExportedCustomDirectEventTypeConstants() {
        return MapBuilder.of(
                "recording",
                MapBuilder.of("registrationName", "onRecording"),
                "locationupdate",
                MapBuilder.of("registrationName", "onLocationUpdate")
        );
    }
}
