import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Text,
  ImageBackground,
  ScrollView,
  TouchableHighlight
} from "react-native";

import MapView from 'react-native-maps';

class DriveDetailScreen extends Component {
  static navigationOptions = {
    headerTitle: <View><Text>1:00 pm - 1:16 pm</Text><Text>June 11th 2018</Text></View>,
  };

  render() {
    return (
      <ScrollView style={styles.container}>
        <ImageBackground source={require('./drive.png')}
            resizeMode='cover'
            style={styles.backdrop}>
          <View style={styles.driveOverlay}>
            <Text style={styles.driveTitle}>Kuala Lumpur</Text>
            <Text style={styles.driveDesc}>1:00 pm - 1:16 pm from Kuala Lumpur</Text>

            <TouchableHighlight
              style={styles.replayButton}>
              <Text style={styles.replayButtonText}>Replay this drive</Text>
            </TouchableHighlight>
          </View>
        </ImageBackground>

        <MapView
          style={styles.map}
          initialRegion={{
            latitude: 3.117058,
            longitude: 101.742150,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          }}
        />

        <View style={styles.detailSection}>
          <Text>Details from this drive</Text>
        </View>

        <View style={styles.detail}>
          <View style={styles.detailColumn}>
            <Text style={styles.detailText}>5.8</Text>
            <Text style={styles.detailDesc}>kilometers</Text>
          </View>

          <View style={styles.detailColumn}>
            <Text style={styles.detailText}>15</Text>
            <Text style={styles.detailDesc}>minutes</Text>
          </View>

          <View style={styles.detailColumn}>
            <Text style={styles.detailText}>1</Text>
            <Text style={styles.detailDesc}>point</Text>
          </View>
        </View>

        <View style={styles.speed}>
          <View style={styles.speedDetail}>
            <Text style={styles.speedDetailTitle}>Average speed</Text>
            <Text style={styles.speedDetailDesc}>22 kph</Text>
          </View>
          <View style={styles.speedDetail}>
            <Text style={styles.speedDetailTitle}>Highest speed</Text>
            <Text style={styles.speedDetailDesc}>70 kph</Text>
          </View>
        </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  backdrop: {
    height: 200,
    flexDirection: 'column',
  },
  driveOverlay: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
    padding: 20,
  },
  driveTitle: {
    fontWeight: 'bold',
    color: "white",
    fontSize: 20,
  },
  driveDesc: {
    color: "white",
    fontSize: 14,
    paddingTop: 4,
  },
  replayButton: {
    marginTop: 32,
    height: 40,
    backgroundColor: 'blue',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  replayButtonText: {
    fontWeight: 'bold',
    fontSize: 14,
    color: 'white',
  },
  map: {
    height: 128,
  },
  detailSection: {
    padding: 12,
    borderBottomWidth: 0.2,
  },
  detail: {
    height: 60,
    borderBottomWidth: 0.2,
    flexDirection: 'row',
    alignItems: 'center',
  },
  detailText: {
    color: "black",
    fontSize: 16,
  },
  detailDesc: {
    fontSize: 12,
  },
  speed: {
    height: 80, 
    padding: 12,
    borderBottomWidth: 0.2,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  speedDetail: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  speedDetailTitle: {
  },
  speedDetailDesc: {
    marginLeft: 8,
    color: 'black',
  },
  detailColumn: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
  }
});

export default DriveDetailScreen;

