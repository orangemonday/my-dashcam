import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  SectionList,
  NativeModules,
} from "react-native";

import { GoogleSignin } from 'react-native-google-signin';

const MDModule = NativeModules.MDModule;

export default class SettingScreen extends Component {
  static navigationOptions = {
    title: 'Settings',
  };

  constructor(props) {
    super(props)
    this.state = {
      accountInfo: [
        <Text style={styles.itemText}></Text>,
        <Text style={styles.itemText}></Text>,
      ],
      setting: [
        <TouchableOpacity
          onPress={this._signOut}
        >
          <Text style={styles.itemText}>Log Out</Text>
        </TouchableOpacity>,
      ]
    }
  }

  async componentWillMount() {
    const { username, email } = await MDModule.getUser();
    console.warn(await MDModule.getUser());
    this.setState({
      accountInfo: [
        <Text style={styles.itemText}>{username}</Text>,
        <Text style={styles.itemText}>{email}</Text>,
      ]
    });
  }
  
  _signOut = async () => {
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
      MDModule.removeUser();
      this.props.navigation.navigate('Auth');
    } catch (error) {
      // do nothing here
    }
  }

  _renderItem = ({item}) => {
    return (
      <View style={styles.item}>
        {item}
      </View>
    )
  }

  _renderSectionHeader({section: {title}}) {
    return (
      <View style={styles.sectionHeader}>
        <Text>{title}</Text>
      </View>
    )
  };

  _renderFooter() {
    return (
      <View style={styles.footer}>
        <Text>v0.1</Text>
        <Text>Made in China</Text>
      </View>
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <SectionList
          renderItem={(this._renderItem)}
          renderSectionHeader={(this._renderSectionHeader)}
          sections={[
            {title: 'Account Info', data: this.state.accountInfo},
            {title: 'Settings', data: this.state.setting},
          ]}
          keyExtractor={(item, index) => item + index}
          ListFooterComponent={this._renderFooter}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 8,
  },
  sectionHeader: {
    height: 48,
    justifyContent: 'center',
    paddingLeft: 16,
    paddingRight: 16,
  },
  sectionHeaderText: {
  },
  item: {
    height: 48, 
    justifyContent: 'center',
    borderBottomWidth: 0.5,
    borderColor: "#bbbbbb",
    paddingLeft: 16,
    paddingRight: 16,
  },
  itemText: {
    color: 'black',
  },
  footer: {
    padding: 16,
  },
});
