package com.mydashcam.camera;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.uimanager.NativeViewHierarchyManager;
import com.facebook.react.uimanager.UIBlock;
import com.facebook.react.uimanager.UIManagerModule;

public class CameraModule extends ReactContextBaseJavaModule {

    public CameraModule(ReactApplicationContext context) {
        super(context);
    }

    @Override
    public String getName() {
        return "MDCameraModule";
    }

    @ReactMethod
    public void startRecording(final int viewTag, final Promise promise) {
        final ReactApplicationContext context = getReactApplicationContext();
        UIManagerModule uiManager = context.getNativeModule(UIManagerModule.class);

        uiManager.addUIBlock(new UIBlock() {
            @Override
            public void execute(NativeViewHierarchyManager nativeViewHierarchyManager) {
                final MDCameraView cameraView;

                cameraView = (MDCameraView) nativeViewHierarchyManager.resolveView(viewTag);
                cameraView.startRecording(promise);
            }
        });
    }

    @ReactMethod
    public void stopRecording(final int viewTag, final Promise promise) {
        final ReactApplicationContext context = getReactApplicationContext();
        UIManagerModule uiManager = context.getNativeModule(UIManagerModule.class);

        uiManager.addUIBlock(new UIBlock() {
            @Override
            public void execute(NativeViewHierarchyManager nativeViewHierarchyManager) {
                final MDCameraView cameraView;

                cameraView = (MDCameraView) nativeViewHierarchyManager.resolveView(viewTag);
                cameraView.stopRecording();
            }
        });
    }
}
