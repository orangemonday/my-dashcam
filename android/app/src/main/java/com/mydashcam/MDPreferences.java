package com.mydashcam;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class MDPreferences {
    static final String PREF = "settings";
    static final String PREF_USERNAME = "username";
    static final String PREF_EMAIL = "email";
    static final String PREF_IDTOKEN = "idToken";

    static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(PREF, Context.MODE_PRIVATE);
    }

    public static void setUser(Context context, String username, String email, String idToken) {
        Editor editor = getSharedPreferences(context).edit();

        editor.putString(PREF_USERNAME, username);
        editor.putString(PREF_EMAIL, email);
        editor.putString(PREF_IDTOKEN, idToken);

        editor.commit();
    }

    public static String getUsername(Context context) {
        return getSharedPreferences(context)
                .getString(PREF_USERNAME, null);
    }

    public static String getEmail(Context context) {
        return getSharedPreferences(context)
                .getString(PREF_EMAIL, null);
    }

    public static String getIdToken(Context context) {
        return getSharedPreferences(context)
                .getString(PREF_IDTOKEN, null);
    }

    public static void removeUser(Context context) {
        Editor editor = getSharedPreferences(context).edit();
        editor.clear();
        editor.commit();
    }

    public static boolean isLoggedIn(Context context) {
        return getSharedPreferences(context).contains(PREF_IDTOKEN);
    }
}
