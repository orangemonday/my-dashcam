package com.mydashcam.camera;

import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;

public class MDCaptureCallback extends CameraCaptureSession.CaptureCallback {
    private static final String TAG = "MDCaptureCallback";
    private DataWriter mDataWriter;

    public MDCaptureCallback(DataWriter dataWriter) {
        mDataWriter = dataWriter;
    }

    @Override
    public void onCaptureCompleted(CameraCaptureSession session, CaptureRequest request, TotalCaptureResult result) {
        mDataWriter.onCaptureCompleted(result);

        super.onCaptureCompleted(session, request, result);
    }
}
