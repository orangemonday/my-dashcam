import {
  API_URL,
  WEBCLIENT_ID,
} from 'react-native-dotenv'

module.exports = {
  API_URL: API_URL,
  WEBCLIENT_ID: WEBCLIENT_ID,
}

