package com.mydashcam;

import android.content.Context;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MDDataDirectory {

    @NonNull
    public static File getLargestStorage(Context context) {
        File[] dirs = context.getExternalFilesDirs(null);

        List<File> mountedDirs = new ArrayList<>();
        for (File dir: dirs) {
            if (Environment.getExternalStorageState(dir)
                    .equals(Environment.MEDIA_MOUNTED));
            mountedDirs.add(dir);
        }

        File storageDir = null;
        long previousFreeSpace = 0;
        for (File dir: mountedDirs) {
            if (dir.getFreeSpace() > previousFreeSpace) {
                storageDir = dir;
                previousFreeSpace = dir.getFreeSpace();
            }
        }

        if (storageDir == null) {
            Log.e("MD", "dataDir is null");
        }
        return storageDir;
    }

    public static List<File> getAllStorageDir(Context context) {
        File[] dirs = context.getExternalFilesDirs(null);
        List<String> strStates = Arrays.asList(
                Environment.MEDIA_MOUNTED,
                Environment.MEDIA_MOUNTED_READ_ONLY,
                Environment.MEDIA_UNKNOWN);

        List<File> extDirs = new ArrayList<>();
        for (File dir: dirs) {
            if (strStates.contains(Environment.getExternalStorageState(dir))) {
                extDirs.add(dir);
            }
        }

        // extDirs.add(new File("/storage/emulated/0"));

        return extDirs;
    }

    public static File getDataDir(Context context) {
        final File dir = new File(getLargestStorage(context), "dashcamData");
        if (!dir.exists()) {
            dir.mkdirs();
        }
        return dir;
    }

    public static List<File> getAllDataDirs(Context context) {
        List<File> extDirs = getAllStorageDir(context);

        List<File> dataDirs = new ArrayList<>();
        for (File dir: extDirs) {
            File dataDir = new File(dir, "dashcamData");
            if (dataDir.isDirectory()) {
                dataDirs.add(dataDir);
            }
        }

        return dataDirs;
    }

    public static long getDataDirFreeSpace(Context context) {
        return getDataDir(context).getFreeSpace();
    }

    public static int getPendingUpload(Context context) {
        List<File> dataDirs = getAllDataDirs(context);
        List<File> driveDirs = new ArrayList<>();
        List<File> cameraFiles = new ArrayList<>();

        // Iterate every data directory
        for (File dir: dataDirs) {
            File[] files = dir.listFiles();

            if (files != null) {
                // Get all drives in data directory
                for (int i = 0; i < files.length; i++) {
                    if (files[i].isDirectory()) {
                           driveDirs.add(files[i]);
                    }
                }

                // Iterate every drives in directory
                for (File driveDir: driveDirs) {
                    File[] driveFiles = driveDir.listFiles();
                    if (driveFiles != null) {
                        // Iterate every file in drives
                        for (int i = 0; i < driveFiles.length; i++) {
                            if (driveFiles[i].getName().equals("acamera")) {
                                cameraFiles.add(driveFiles[i]);
                            }
                        }
                    }
                }

            }
        }

        return cameraFiles.size();
    }
}
