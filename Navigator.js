import { createStackNavigator, createSwitchNavigator } from 'react-navigation';

import MainScreen from "./MainScreen";
import NewDriveScreen from "./NewDriveScreen";
import DriveDetailScreen from "./DriveDetailScreen";
import LoginScreen from "./LoginScreen.js";
import SettingScreen from './SettingScreen.js';
import AuthLoadingScreen from './AuthLoadingScreen.js';
import TOSScreen from './TOSScreen.js';
import RegisterScreen from './RegisterScreen.js';

const AppStack = createStackNavigator({
  Home: MainScreen,
  NewDrive: NewDriveScreen,
  DriveDetail: DriveDetailScreen,
  Setting: SettingScreen,
}, {
  headerMode: 'screen',
  initialRouteName: 'Home',
});

const AuthStack = createStackNavigator({
  Login: LoginScreen,
  TOS: TOSScreen,
  Register: RegisterScreen,
}, {
  headerMode: 'screen',
});

export default createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    App: AppStack,
    Auth: AuthStack,
  },
  {
    initialRouteName: 'AuthLoading',
  }
);

