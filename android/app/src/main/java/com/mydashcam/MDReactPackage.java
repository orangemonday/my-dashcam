package com.mydashcam;

import com.facebook.react.ReactPackage;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.uimanager.ViewManager;
import com.mydashcam.camera.CameraModule;
import com.mydashcam.camera.MDCameraViewManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MDReactPackage implements ReactPackage {

    @Override
    public List<ViewManager> createViewManagers(ReactApplicationContext reactContext) {
        List<ViewManager> viewManagers = new ArrayList<>();

        viewManagers.add(new MDCameraViewManager());

        return viewManagers;
    }

    @Override
    public List<NativeModule> createNativeModules(ReactApplicationContext reactContext) {
        List<NativeModule> modules = new ArrayList<>();

        modules.add(new CameraModule(reactContext));
        modules.add(new MDModule(reactContext));

        return modules;
    }
}
