package com.mydashcam.camera;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.SurfaceTexture;
import android.util.Log;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;

import com.mydashcam.R;

public class TextureViewPreview {
    interface Callback {
        void onSurfaceAvailable();
        void onSurfaceChanged();
        void onSurfaceDestroyed();
    }

    private Callback mCallback;

    private final AutoFitTextureView mTextureView;

    private int mWidth;
    private int mHeight;

    private int mRatioWidth;
    private int mRatioHeight;

    private int mDisplayOrientation;

    TextureViewPreview(Context context, ViewGroup parent) {
        final View view = View.inflate(context, R.layout.texture_view, parent);
        mTextureView = (AutoFitTextureView) view.findViewById(R.id.texture_view);

        mTextureView.setSurfaceTextureListener(new TextureView.SurfaceTextureListener() {
            @Override
            public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
                Log.d("MDCamera", "onSurfaceTextureAvailable");
                setSize(width, height);
                configureTransform();
                mCallback.onSurfaceAvailable();
            }

            @Override
            public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
                Log.d("MDCamera", "onSurfaceTextureSizeChanged");
                setSize(width, height);
                configureTransform();
                mCallback.onSurfaceChanged();
            }

            @Override
            public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
                mCallback.onSurfaceDestroyed();
                return true;
            }

            @Override
            public void onSurfaceTextureUpdated(SurfaceTexture surface) {
            }
        });
    }

    public void setDisplayOrientation(int displayOrientation) {
        mDisplayOrientation = displayOrientation;
        configureTransform();
    }

    public void configureAspectRatio() {
        if (mDisplayOrientation % 180 == 90) {
            mTextureView.setAspectRatio(mWidth, mHeight);
        } else {
            mTextureView.setAspectRatio(mHeight, mWidth);
        }
    }

    public void setAspectRatio(int width, int height) {
        mRatioWidth = width;
        mRatioHeight = height;
        configureTransform();
    }

    private void configureTransform() {
        Matrix matrix = new Matrix();
        if (mDisplayOrientation % 180 == 90) {
            final int width = getWidth();
            final int height = getHeight();
            float startWidth = 0.f;
            float endWidth = width;
            if (width > mRatioWidth) {
                startWidth = (width - mRatioWidth) / 2.f;
                endWidth = startWidth + mRatioWidth;
            }
            // Rotate the camera preview when the screen is landscape.
            matrix.setPolyToPoly(
                    new float[]{
                            0.f, 0.f, // top left
                            width, 0.f, // top right
                            0.f, height, // bottom left
                            width, height, // bottom right
                    }, 0,
                    mDisplayOrientation == 90 ?
                            // Clockwise
                            new float[]{
                                    startWidth, height, // top left
                                    startWidth, 0.f, // top right
                                    endWidth, height, // bottom left
                                    endWidth, 0.f, // bottom right
                            } : // mDisplayOrientation == 270
                            // Counter-clockwise
                            new float[]{
                                    endWidth, 0.f, // top left
                                    endWidth, height, // top right
                                    startWidth, 0.f, // bottom left
                                    startWidth, height, // bottom right
                            }, 0,
                    4);
        } else if (mDisplayOrientation == 180) {
            matrix.postRotate(180, getWidth() / 2, getHeight() / 2);
        }

        Log.d("MDCamera", "configureTransform: " + mDisplayOrientation + " " + getWidth() + "x" + getHeight());
        Log.d("MDCamera", "configureTransform: " + mRatioWidth + "x" + mRatioHeight);
        mTextureView.setTransform(matrix);
        configureAspectRatio();
    }

    public SurfaceTexture getSurfaceTexture() {
        return mTextureView.getSurfaceTexture();
    }

    public View getView() {
        return mTextureView;
    }

    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    public boolean isReady() {
        return mTextureView.getSurfaceTexture() != null;
    }

    private void setSize(int width, int height) {
        mWidth = width;
        mHeight = height;
    }

    private int getWidth() {
        return mWidth;
    }

    private int getHeight() {
        return mHeight;
    }
}
