import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  NativeModules,
  TouchableHighlight,
} from "react-native";

const MDModule = NativeModules.MDModule;

export default class TOSScreen extends Component {
  static navigationOptions = {
    header: null,
  }

  _accept = async () => {
    const { navigation } = this.props;
    const json = navigation.getParam('json', null);
    MDModule.setUser(json.name, json.email, json.token);

    this.props.navigation.navigate("App")
  }

  render() {
    return (
      <ScrollView style={styles.container}>
        <Text style={[styles.title, styles.space]}>Terms of Service</Text>

        <Text style={styles.desc}>The Terms and Conditions below are effective for all users</Text>
        <Text style={[styles.desc, styles.space]}>Last Updated on July 16, 2018</Text>

        <Text style={[styles.desc, styles.space]}>
          Please read these Terms and Conditions ("Terms", 
          "Terms and Conditions") carefully before using mydashcam
          (the "Software"), a software offered by MyDashcam, Inc.
        </Text>

        <View>
          <TouchableHighlight
            style={styles.button}
            onPress={this._accept}>
            <Text>Accept</Text>
          </TouchableHighlight>

          <TouchableHighlight
            style={styles.button}
            onPress={() => {}}>
            <Text>Decline</Text>
          </TouchableHighlight>
        </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 16,
  },
  desc: {
    fontSize: 12,
  },
  space: {
    marginBottom: 8,
  },
  button: {
    marginTop: 16,
    padding: 16,
    borderRadius: 16,
    borderWidth: 1,
    alignItems: 'center',
  }
});

