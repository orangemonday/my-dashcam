package com.mydashcam.camera;

import android.content.Context;
import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaCodecList;
import android.media.MediaFormat;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.Surface;

import java.io.IOException;
import java.nio.ByteBuffer;

public class MDEncoder extends MediaCodec.Callback {
    private HandlerThread mHandlerThread;
    private Handler mHandler;

    private MediaFormat mMediaFormat;
    private MediaCodec mEncoder;
    private MediaCodecList mMediaCodecList;
    private String mStrFormat;

    private Surface mInputSurface;

    private Context mContext;

    private DataWriter mDataWriter;

    private boolean mIsReady = false;

    private static final String TAG = "MDEncoder";

    public MDEncoder(Context context, DataWriter dataWriter) {
        mContext = context;
        mDataWriter = dataWriter;

        mHandlerThread = new HandlerThread("encode_handler");
        mHandlerThread.start();

        mHandler = new Handler(mHandlerThread.getLooper());

        mMediaFormat = MediaFormat.createVideoFormat(
                            MediaFormat.MIMETYPE_VIDEO_AVC,
                            640,480);
        // 4 Megabits
        mMediaFormat.setInteger(MediaFormat.KEY_BIT_RATE, 0x400000);
        mMediaFormat.setInteger(MediaFormat.KEY_BITRATE_MODE,
                MediaCodecInfo.EncoderCapabilities.BITRATE_MODE_VBR);
        mMediaFormat.setInteger(MediaFormat.KEY_COLOR_FORMAT,
                MediaCodecInfo.CodecCapabilities.COLOR_FormatSurface);

        mMediaFormat.setFloat(MediaFormat.KEY_FRAME_RATE, 30);
        mMediaFormat.setInteger(MediaFormat.KEY_I_FRAME_INTERVAL, 1);
        mMediaFormat.setInteger(MediaFormat.KEY_PROFILE,
                MediaCodecInfo.CodecProfileLevel.AACObjectLC);
        mMediaFormat.setInteger(MediaFormat.KEY_LEVEL, MediaCodecInfo.CodecProfileLevel.AVCLevel4);
        // mMediaFormat.setInteger(MediaFormat.KEY_MAX_INPUT_SIZE, );

        mMediaCodecList = new MediaCodecList(MediaCodecList.ALL_CODECS);
        mStrFormat = mMediaCodecList.findEncoderForFormat(mMediaFormat);

        if (mStrFormat == null) {
            Log.d(TAG, "Unable to find encoder format");
            return;
        }


        try {
            StringBuilder sb = new StringBuilder();
            sb.append("encoderName ");
            sb.append(mStrFormat);
            Log.d(TAG, "createByCodecName for " + sb.toString());

            mEncoder = MediaCodec.createByCodecName(mStrFormat);
        } catch (IOException e) {
            e.printStackTrace();
            Log.d(TAG, "createByCodecName failed");
        }


        mInputSurface = MediaCodec.createPersistentInputSurface();
        configure();
    }

    public Surface getInputSurface() {
        return mInputSurface;
    }

    @Override
    public void onInputBufferAvailable(@NonNull MediaCodec codec, int index) {
        Log.d(TAG, "onInputBufferAvailable");
    }

    @Override
    public void onOutputBufferAvailable(@NonNull final MediaCodec codec, final int index, @NonNull final MediaCodec.BufferInfo info) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                ByteBuffer outputBuffer = codec.getOutputBuffer(index);
                outputBuffer.position(info.offset);
                outputBuffer.limit(info.offset + info.size);

                boolean config = false;
                if ((info.flags & MediaCodec.BUFFER_FLAG_CODEC_CONFIG) != 0) {
                    config = true;
                    Log.d(TAG, "CODEC_CONFIG");
                }

                boolean keyframe = false;
                if ((info.flags & MediaCodec.BUFFER_FLAG_KEY_FRAME) != 0) {
                    keyframe = true;
                    Log.d(TAG, "KEY_FRAME");
                }

                mDataWriter.writeVideoBuffer(info.presentationTimeUs, outputBuffer, config, keyframe);

                codec.releaseOutputBuffer(index, false);

                if ((info.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
                    Log.d(TAG, "END_OF_STREAM");
                    mIsReady = false;
                }
            }
        });
    }

    @Override
    public void onError(@NonNull MediaCodec codec, @NonNull MediaCodec.CodecException e) {
        Log.e(TAG, "onError: " + e);
    }

    @Override
    public void onOutputFormatChanged(@NonNull MediaCodec codec, @NonNull MediaFormat format) {
        mMediaFormat = format;
        Log.d(TAG, "onOutputFormatChanged: " + format);
    }

    private void reset() {
        if (mEncoder != null) {
            try {
                Log.d(TAG, "flush");
                mEncoder.flush();
                Log.d(TAG, "reset");
                mEncoder.reset();
            } catch (IllegalStateException e) {
                Log.d(TAG, "reset: MediaCodec have not started yet");
            }
        }
    }

    public void configure() {
        reset();
        Log.d(TAG, "after reset");
        // TODO: Cant record the 2nd time, when recording at the second time, it crashed after reset() or stop().
        try {
            mEncoder.configure(mMediaFormat, null, null,
                    MediaCodec.CONFIGURE_FLAG_ENCODE);
        } catch (MediaCodec.CodecException e) {
            Log.e(TAG, "ERROR " + e.getDiagnosticInfo());
        }
        if (mInputSurface != null && mInputSurface.isValid()) {
            mEncoder.setInputSurface(mInputSurface);
        }
        mEncoder.setCallback(this);
        mEncoder.start();
        mIsReady = true;
    }

    public boolean isReady() {
        return mIsReady;
    }

    public void end() {
        mEncoder.signalEndOfInputStream();
    }
}
