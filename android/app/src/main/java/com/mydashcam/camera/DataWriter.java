package com.mydashcam.camera;

import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.ColorSpaceTransform;
import android.hardware.camera2.params.RggbChannelVector;
import android.os.SystemClock;
import android.util.Log;
import android.util.Pair;

import com.mydashcam.cereal.Event;
import com.mydashcam.cereal.Logger;

import org.capnproto.PrimitiveList;
import org.capnproto.SerializePacked;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class DataWriter {

    private final String  TAG = "DataWriter";


    private int mSegmentCount;

    private File mRootDir;
    private File mSegmentDir;
    private String mDriveDirStr;

    private File mLogFile;
    private File mVideoFile;

    private WritableByteChannel mLogChannel;
    private WritableByteChannel mVideoChannel;

    private byte[] configBytes;
    private long mLastCaptureCompletedTime;
    private int mIntraFrameIndex;
    private boolean mIsNextSegment;
    private int mSegmentFrameIndex;
    private final int MAX_FRAME = 100;

    private ArrayDeque<Pair<Long, Long>> mFrameIds;
    private ArrayList<Event> mFrameEvents;

    public DataWriter() {
        configBytes = null;
        mFrameIds = new ArrayDeque();
        mFrameEvents = new ArrayList();

        mLastCaptureCompletedTime = 0L;
    }

    public void startWriting(File dir) {
        mSegmentCount = -1;

        mRootDir = dir;

        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd--HH-mm-ss", Locale.US);
        mDriveDirStr = dateFormat.format(new Date());

        nextSegment();
        nextCameraFile();
        mIsNextSegment = false;
    }

    private void createLogFile() {
        mSegmentDir = new File(mRootDir, mDriveDirStr + "--" + mSegmentCount);
        mSegmentDir.mkdirs();

        String logFilename = "log";
        mLogFile = new File(mSegmentDir, logFilename);

        try {
            FileOutputStream logStream = new FileOutputStream(mLogFile);
            mLogChannel = logStream.getChannel();

            Log.d(TAG, "createLogFile " + mLogFile.getPath());
        } catch (FileNotFoundException e) {
            Log.e(TAG, "Cannot create log output channel");
        }
    }

    private void nextCameraFile() {
        String videoFilename = "acamera";
        try {
            if (mVideoChannel != null) {
                mVideoChannel.close();
                mVideoChannel = null;
            }
        } catch (IOException e) {
            Log.e(TAG, "Cannot close previous video output channel");
        }

        try {
            mVideoFile = new File(mSegmentDir, videoFilename);
            FileOutputStream videoStream = new FileOutputStream(mVideoFile);

            if (configBytes != null) {
                videoStream.write(configBytes);
            }
            mVideoChannel = videoStream.getChannel();

            Log.d(TAG, "nextCameraFile " + mVideoFile.getPath());
            mSegmentFrameIndex = 0;
        } catch (FileNotFoundException e) {
            Log.e(TAG, "Cannot create camera output channel");
        } catch (IOException e) {
            Log.e(TAG, "Cannot write configBytes");
        }
    }

    private void nextSegment() {
        mSegmentCount++;

        try {
            if (mLogChannel != null) {
                mLogChannel.close();
                mLogChannel = null;
            }
        } catch (IOException e) {
            Log.e(TAG, "Cannot close previous log output channel");
        }

        createLogFile();
    }

    public void rotate() {
        mIsNextSegment = true;
    }

    public void writeEvent(Event event) {
        try {
            SerializePacked.writeToUnbuffered(mLogChannel, event.message);
        } catch (IOException e) {
            Log.e(TAG, "Cannot write message to Capnp Serializer");
        }
    }

    public void writeFrameEvent(Event event) {
        if (mIsNextSegment) {
            mFrameEvents.add(event);
        } else {
            writeEvent(event);
        }
    }

    public void onCaptureCompleted(TotalCaptureResult result) {
        mLastCaptureCompletedTime = SystemClock.elapsedRealtimeNanos();

        // Add FrameId to list
        long frame = result.getFrameNumber();
        long nanoTime = result.get(CaptureResult.SENSOR_TIMESTAMP);
        Pair<Long, Long> frameId = new Pair(frame, nanoTime);
        addFrame(frameId);

        // Log FrameData event
        Event  e = new Event();
        Logger.Event.Builder builder = e.builder;
        Logger.FrameData.Builder frameData = builder.initFrameData();
        frameData.setFrameId((int) frame);
        frameData.setTimestampEof(nanoTime);

        Logger.FrameData.AndroidCaptureResult.Builder androidCaptureResult = frameData.initAndroidCaptureResult();

        Integer sensorSensitivity = result.get(CaptureResult.SENSOR_SENSITIVITY);
        if (sensorSensitivity != null) {
            androidCaptureResult.setSensitivity(sensorSensitivity);
        }
        Long sensorExposureTime = result.get(CaptureResult.SENSOR_EXPOSURE_TIME);
        if (sensorExposureTime != null) {
            androidCaptureResult.setExposureTime(sensorExposureTime);
        }
        Long sensorFrameDuration = result.get(CaptureResult.SENSOR_FRAME_DURATION);
        if (sensorFrameDuration != null) {
            androidCaptureResult.setFrameDuration(sensorFrameDuration);
        }
        Long sensorRollingShutterSkew = result.get(CaptureResult.SENSOR_ROLLING_SHUTTER_SKEW);
        if (sensorRollingShutterSkew != null) {
            androidCaptureResult.setRollingShutterSkew(sensorRollingShutterSkew);
        }

        RggbChannelVector ccg = result.get(CaptureResult.COLOR_CORRECTION_GAINS);
        if (ccg != null) {
            PrimitiveList.Float.Builder colorCorrectionGains = androidCaptureResult.initColorCorrectionGains(4);
            float []ccgArray = new float[4];
            ccg.copyTo(ccgArray, 0);
            for (int i = 0; i < 4; i ++) {
                androidCaptureResult.getColorCorrectionGains().set(i, ccgArray[i]);
            }
        }

        ColorSpaceTransform cct = result.get(CaptureResult.COLOR_CORRECTION_TRANSFORM);
        if (cct != null) {
            PrimitiveList.Int.Builder colorCorrectionTransform = androidCaptureResult.initColorCorrectionTransform(18);
            int []cctArray = new int [18];
            cct.copyElements(cctArray, 0);
            for (int i =0; i < 18; i++) {
                androidCaptureResult.getColorCorrectionTransform().set(i, cctArray[i]);
            }
        }

        writeFrameEvent(e);
//          Log.d(TAG, "onCaptureCompleted frame: " + frame + " time: " + nanoTime );
    }

    private void addFrame(Pair<Long, Long> frameId) {
        synchronized (mFrameIds) {
            mFrameIds.addLast(frameId);
            if (mFrameIds.size() >= MAX_FRAME) {
                mFrameIds.removeFirst();
            }

            mFrameIds.notify();
        }
    }

    public Pair<Long, Long> findCorrespondingFrameId(Long presentationTimeUs) {
        synchronized (mFrameIds) {
//            Log.d(TAG, "findCorrespondingFrameId: size " + mFrameIds.size());
            for (Iterator<Pair<Long, Long>> it = mFrameIds.iterator(); it.hasNext(); ) {
                Pair<Long, Long> pair = it.next();

                if (Math.abs(pair.second - presentationTimeUs) <= 2000000L) {
                    mFrameIds.remove(pair);
//                    Log.d(TAG, "findCorrespondingFrameId: Found frame " + pair.first);
                    return pair;
                }
            }

            // 300000000L 100% no frame lost
            if ( (mFrameIds.size() < MAX_FRAME) &&
                    (SystemClock.elapsedRealtimeNanos() - mLastCaptureCompletedTime) < 200000000L) {
                Log.d(TAG, "findCorrespondingFrameId: Wait for corresponding frame " + presentationTimeUs);
                try {
                    mFrameIds.wait(200L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                for (Iterator<Pair<Long, Long>> it = mFrameIds.iterator(); it.hasNext(); ) {
                    Pair<Long, Long> pair = it.next();

                    if (Math.abs(pair.second - presentationTimeUs) <= 2000000L) {
                        mFrameIds.remove(pair);
//                        Log.d(TAG, "findCorrespondingFrameId: Found frame " + pair.first);
                        return pair;
                    }
                }
            }
            else {
                Log.e(TAG, "findCorrespondingFrameId: Cannot find corresponding frame " + presentationTimeUs);
            }
        }

        return null;
    }

    public void writeVideoBuffer(long presentationTimeUs, ByteBuffer buffer, boolean isConfig, boolean isKeyframe) {
        // Save config bytes for future segment initialization
        int frameNum = -1;
        if (isConfig) {
            int prevPosition = buffer.position();
            this.configBytes = new byte[buffer.remaining()];
            buffer.get(this.configBytes);
            buffer.position(prevPosition);
        }
        else {
//            Log.d(TAG, "writeVideoBuffer time: " + presentationTimeUs * 1000L);
            Pair<Long, Long> frameId = findCorrespondingFrameId(presentationTimeUs * 1000L);
            if (frameId != null) {
                frameNum = frameId.first.intValue();
            }
        }

        if (isKeyframe) {
            mIntraFrameIndex = 0;
        } else {
            mIntraFrameIndex++;
        }

        if (mIsNextSegment && (isKeyframe || mIntraFrameIndex > 100)) {
            int n = 0, curEnd = 0;
            boolean found = false;
            for (Iterator<Event> it = mFrameEvents.iterator(); it.hasNext(); n++) {
                Event e = it.next();
                if (e.builder.isFrameData() && e.builder.getFrameData().getFrameId() > frameNum) {
                    curEnd = n + 1;
                    found = true;
                    break;
                }
            }

            if (found) {
                List<Event> curLog = mFrameEvents.subList(0, curEnd);
                for (Event e: curLog) {
                    writeEvent(e);
                }
            }

            nextSegment();
            nextCameraFile();
            mIsNextSegment = false;

            List<Event> nextLog = mFrameEvents.subList(curEnd, mFrameEvents.size());
            for (Event e: nextLog) {
                writeEvent(e);
            }
            mFrameEvents.clear();
        }

        try {
            mVideoChannel.write(buffer);

            if (!isConfig) {
                Event e = new Event();
                Logger.EncodeIndex.Builder encodeIndex = e.builder.initEncodeData();
                encodeIndex.setFrameId(frameNum);
                encodeIndex.setType(Logger.EncodeIndex.Type.CHFFR_ANDROID_H264);
                encodeIndex.setSegmentNum(mSegmentCount);
                encodeIndex.setSegmentId(mSegmentFrameIndex);

                writeFrameEvent(e);
                mSegmentFrameIndex++;
            }
        } catch (IOException e) {
            Log.e(TAG, "writeToFile error " + e);
        }
    }
}
