import React from 'react';
import PropTypes from 'prop-types';
import {
  requireNativeComponent,
  ViewPropTypes,
  NativeModules,
  findNodeHandle,
} from 'react-native';

const CameraManager = NativeModules.MDCameraModule;

class Camera extends React.Component {
  constructor(props) {
    super(props);
  }

  async startRecording() {
    return await CameraManager.startRecording(this._cameraHandle);
  }

  stopRecording() {
    CameraManager.stopRecording(this._cameraHandle);
  }

  _onRecording(event: Event) {
    if (!this.props.onRecordingMessage) {
      return;
    }
    this.props.onRecordingMessage(event.nativeEvent);
  }

  _onLocationUpdate(event: Event) {
    if (!this.props.onLocationUpdateMessage) {
      return;
    }
    this.props.onLocationUpdateMessage(event.nativeEvent);
  }

  _setReference = (ref) => {
    this._cameraRef = ref;
    this._cameraHandle = findNodeHandle(ref);
  }

  render() {
    return (
      <MDCamera
        style={{flex: 1}}
        ref={this._setReference}
        onRecording={this._onRecording.bind(this)}
        onLocationUpdate={this._onLocationUpdate.bind(this)}
      />
    )
  }
}

Camera.propTypes = {
  onRecordingMessage: PropTypes.func,
  onLocationUpdateMessage: PropTypes.func,
}

const MDCamera = requireNativeComponent('MDCamera', Camera,
  {nativeOnly: {
    testID: true,
    renderToHardwareTextureAndroid: true,
    accessibilityLabel: true,
    importantForAccessibility: true,
    accessibilityLiveRegion: true,
    accessibilityComponentType: true,
    onLayout: true,
    nativeID: true
}});

export default Camera;

