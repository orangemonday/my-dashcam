import React, { Component } from "react";
import {
  StyleSheet,
  View,
  NativeModules,
} from 'react-native';

import { GoogleSignin } from 'react-native-google-signin';

import Config from './config.js'

const MDModule = NativeModules.MDModule;

export default class AuthLoadingScreen extends Component {
  constructor(props) {
    super(props)
  }

  async componentDidMount() {
    await GoogleSignin.configure({
      scopes: [],
      offlineAccess: false,
      webClientId: Config.WEBCLIENT_ID,
      forceConsentPrompt: true,
    });
    await this._bootstrapAsync();
  }

  _bootstrapAsync = async () => {
    try {
      await MDModule.isLoggedIn();
      this.props.navigation.navigate('App');
    } catch (error) {
      this.props.navigation.navigate('Auth');
    }
  };

  render() {
    return (
      <View style={styles.container}/>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'red',
  },
});

