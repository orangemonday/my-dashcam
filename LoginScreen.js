import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
} from "react-native";

import { GoogleSignin, GoogleSigninButton } from 'react-native-google-signin';

import Config from './config.js'

export default class LoginScreen extends Component {
  static navigationOptions = {
    header: null,
  }

  constructor() {
    super();
    this.state = {
      isError: false,
    }
  }

  _isUserRegistered = async () => {
    try {
    } catch (error) {
    }
  };

  _signIn = async () => {
    try {
      const user = await GoogleSignin.signIn();
      let response = await fetch(Config.API_URL + "/auth/signin/", {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          id_token: user.idToken,
        }),
      });
      const json = await response.json();

      if (json.registered) {
        this.props.navigation.navigate("TOS", {json, user});
      } else {
        this.props.navigation.navigate("Register", {json, user});
      }
    } catch (error) {
      this.setState({isError: true});
    }
  };

  _renderButton() {
    const googleButton = <GoogleSigninButton
                            style={{ width: 230, height: 48, marginTop: 16 }}
                            size={GoogleSigninButton.Size.Standard}
                            color={GoogleSigninButton.Color.Light}
                            onPress={this._signIn}/>

    if (this.state.isError) {
      return (
        <View>
          {googleButton}
          <View style={styles.error}>
            <Text style={styles.errorText}>There was a problem logging in.</Text>
            <Text style={styles.errorText}>Please try again.</Text>
          </View>
        </View>
      ) 
    } else {
      return (
        <View>
          {googleButton}
        </View>
      )
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>MyDashcam</Text>
        <Text style={styles.desc}>The dashcam app that teaches self-driving cars</Text>
        {this._renderButton()}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 28,
    fontWeight: 'bold',
    color: 'white',
  },
  desc: {
    margin: 8,
    width: '66%',
    fontSize: 18,
    textAlign: 'center',
    color: 'white',
  },
  error: {
    position: 'absolute',
    top: 64,
    left: 0,
    right: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  errorText: {
    fontSize: 12,
    color: 'white',
  },
});

