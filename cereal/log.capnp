@0x9eb32e19f86ee174;

using Java = import "./include/java.capnp";
$Java.package("com.mydashcam.cereal");
$Java.outerClassname("Logger");

# TODO SensorEventData GPSNMEAData

struct InitData {
  kernelArgs @0 :List(Text);
  gctx @1 :Text;
  dongleId @2 :Text;

  deviceType @3 :DeviceType;
  version @4 :Text;

  androidBuildInfo @5 :AndroidBuildInfo;

  enum DeviceType {
    android @0;
    ios @1;
  }

  struct AndroidBuildInfo {
    board @0 :Text;
    bootloader @1 :Text;
    brand @2 :Text;
    device @3 :Text;
    display @4 :Text;
    fingerprint @5 :Text;
    hardware @6 :Text;
    host @7 :Text;
    id @8 :Text;
    manufacturer @9 :Text;
    model @10 :Text;
    product @11 :Text;
    radioVersion @12 :Text;
    serial @13 :Text;
    supportedAbis @14 :List(Text);
    tags @15 :Text;
    time @16 :Int64;
    type @17 :Text;
    user @18 :Text;

    versionCodename @19 :Text;
    versionRelease @20 :Text;
    versionSdk @21 :Int32;
    versionSecurityPatch @22 :Text;
  }
}

# android struct GpsLocation
struct GpsLocationData {
  # Represents latitude in degrees.
  latitude @0 :Float64;

  # Represents longitude in degrees.
  longitude @1 :Float64;

  # Represents altitude in meters above the WGS 84 reference ellipsoid.
  altitude @2 :Float64;

  # Represents speed in meters per second.
  speed @3 :Float32;

  # Represents heading in degrees.
  bearing @4 :Float32;

  # Represents expected accuracy in meters. (presumably 1 sigma?)
  accuracy @5 :Float32;

  # Timestamp for the location fix.
  # Milliseconds since January 1, 1970.
  timestamp @6 :UInt64;

  source @7 :SensorSource;

  enum SensorSource {
    android @0;
    ios @1;
  }
}

struct FrameData {
  frameId @0 :Int32;
  timestampEof @1 :UInt64;
  androidCaptureResult @2 :AndroidCaptureResult;

  struct AndroidCaptureResult {
      sensitivity @0 :Int32;
      frameDuration @1 :Int64;
      exposureTime @2 :Int64;
      rollingShutterSkew @3 :UInt64;
      colorCorrectionTransform @4 :List(Int32);
      colorCorrectionGains @5 :List(Float32);
      displayRotation @6 :Int8;
  }
}

struct EncodeIndex {
  # picture from camera
  frameId @0 :Int32;
  type @1 :Type;
  # minute long segment this frame is in
  segmentNum @2 :Int32;
  # index into camera file in segment in presentation order
  segmentId @3 :UInt32;

  enum Type {
    bigBoxLossless @0;   # rcamera.mkv
    fullHEVC @1;         # fcamera.hevc
    bigBoxHEVC @2;       # bcamera.hevc
    chffrAndroidH264 @3; # acamera
    fullLosslessClip @4; # prcamera.mkv
    front @5;            # dcamera.hevc
  }
}

# android SensorEvent
struct SensorEventData {
  version    @0 :Int32;
  type       @1 :Int32;
  timestamp  @2 :UInt64;
  source     @3 :SensorSource;

  union {
    acceleration         @4 :SensorVec;
    magnetic             @5 :SensorVec;
    magneticUncalibrated @6 :SensorVec;
    gyro                 @7 :SensorVec;
    gyroUncalibrated     @8 :SensorVec;
    pressure             @9 :SensorVec;
  }

  struct SensorVec {
    v @0 :List(Float32);
    accuracy @1 :Int8;
  }

  enum SensorSource {
    android @0;
    ios @1;
  }
}

struct Event {
  logMonoTime @0 :UInt64;
  # in nanoseconds

  union {
    initData @1 :InitData;
    frameData @2 :FrameData;
    gpsLocationData @3 :GpsLocationData;
    encodeData @4 :EncodeIndex;
    sensorEventData @5 :SensorEventData;
  }
}

