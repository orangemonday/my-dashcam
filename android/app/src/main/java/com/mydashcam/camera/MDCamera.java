package com.mydashcam.camera;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.location.Location;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;
import android.util.Range;
import android.util.Size;
import android.view.Surface;
import android.view.View;

import com.mydashcam.MDDataDirectory;
import com.mydashcam.MDSensorLogger;
import com.mydashcam.cereal.Event;
import com.mydashcam.cereal.Logger;

import org.capnproto.Text;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MDCamera extends CameraViewImpl {

    int FACING_FRONT = 0;
    int FACING_BACK = 1;

    private final TextureViewPreview mPreview;

    private Size mPreviewSize;

    private static final String TAG = "MDCamera";

    private final Context mContext;

    private final CameraManager mCameraManager;

    private final LocationService mLocationService;

    private String mCameraId;

    private CameraCharacteristics mCameraCharacteristics;

    private CaptureRequest.Builder mPreviewBuilder;

    CameraDevice mCamera;

    CameraCaptureSession mCameraCaptureSession;

    private MDEncoder mEncoder;

    private Size mVideoSize;

    private float distanceTravel = 0;

    private Location mPreviousLocation;

    private boolean mIsRecording = false;

    private DataWriter mDataWriter;
    private MDSensorLogger mSensorLogger;

    private Timer mRotateTimer;
    private TimerTask mRotateTask;

    private CameraDevice.StateCallback mCameraDeviceCallback
            = new CameraDevice.StateCallback() {

        @Override
        public void onOpened(@NonNull CameraDevice camera) {
            mCamera = camera;
            startPreviewSession();
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice camera) {
            camera.close();
            mCamera = null;
        }

        @Override
        public void onError(@NonNull CameraDevice camera, int error) {
            Log.e(TAG, "onError: " + camera.getId() + " (" + error + ")");
            mCamera = null;
        }
    };

    private CameraCaptureSession.StateCallback mSessionCallback
            = new CameraCaptureSession.StateCallback() {
        @Override
        public void onConfigured(@NonNull CameraCaptureSession session) {
            mCameraCaptureSession = session;
            updatePreview();
        }

        @Override
        public void onConfigureFailed(@NonNull CameraCaptureSession session) {
            Log.e(TAG, "Failed to configure capture session");
        }

        @Override
        public void onClosed(@NonNull CameraCaptureSession session) {
            mCameraCaptureSession = null;
        }
    };

    private CameraCaptureSession.CaptureCallback mCaptureCallback;

    MDCamera(Callback callback, Context context, TextureViewPreview preview) {
        mCameraManager = (CameraManager) context.getSystemService(Context.CAMERA_SERVICE);

        mContext = context;

        mCallback = callback;

        mPreview = preview;
        mPreview.setCallback(new TextureViewPreview.Callback() {
            @Override
            public void onSurfaceAvailable() {
                start();
            }

            @Override
            public void onSurfaceChanged() {
                startPreviewSession();
            }

            @Override
            public void onSurfaceDestroyed() {
                stop();
            }
        });

        mDataWriter = new DataWriter();
        mCaptureCallback = new MDCaptureCallback(mDataWriter);

        mRotateTask = new RecordingTask();
        mRotateTimer = new Timer("Rotate");
        mEncoder = new MDEncoder(context, mDataWriter);

        mLocationService = new LocationService(context);
        mLocationService.setCallback(new LocationService.Callback() {
            @Override
            public void onLocationChanged(Location location) {
                // Update speed to screen
                float speed = location.getSpeed();

                if (mIsRecording) {
                    Event event = new Event();
                    Logger.Event.Builder builder = event.builder;

                    Logger.GpsLocationData.Builder gpsLocationData =
                            builder.initGpsLocationData();
                    gpsLocationData.setLongitude(location.getLongitude());
                    gpsLocationData.setLatitude(location.getLatitude());
                    gpsLocationData.setAltitude(location.getAltitude());
                    gpsLocationData.setSpeed(speed);
                    gpsLocationData.setBearing(location.getBearing());
                    gpsLocationData.setTimestamp(location.getTime());
                    gpsLocationData.setAccuracy(location.getAccuracy());
                    gpsLocationData.setSource(Logger.GpsLocationData.SensorSource.ANDROID);

                    mDataWriter.writeEvent(event);

                }

                if (mPreviousLocation != null) {
                    distanceTravel += mPreviousLocation.distanceTo(location);
                }
                mCallback.onLocationUpdate(distanceTravel, speed);
                mPreviousLocation = location;
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
            }
        });
    }

    public boolean start() {
        openCamera();
        mLocationService.start();
        return true;
    }

    public void stop() {
        if (mCamera != null) {
            mCamera.close();
            mCamera = null;
        }

        if (mIsRecording) {
            stopRecording();
        }

        stopCaptureSession();

        mLocationService.stop();
    }

    @SuppressLint("MissingPermission")
    private void openCamera() {
        if (chooseBackFacingCamera()) {
            Log.d(TAG, "chooseBackFacingCamera: true");
        }
        try {
            StreamConfigurationMap map = mCameraCharacteristics
                    .get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);

            mVideoSize = chooseVideoSize(map.getOutputSizes(MediaRecorder.class));


            Log.d(TAG, "previewSize: " + mPreview.getView().getWidth() + "x" + mPreview.getView().getHeight());
            mPreviewSize = choosePreviewSize(
                    map.getOutputSizes(SurfaceTexture.class),
                    mPreview.getView().getWidth(),
                    mPreview.getView().getHeight(),
                    mVideoSize);
            Log.d(TAG, "choosePreviewSize: " + mPreviewSize.getWidth() + "x" + mPreviewSize.getHeight());

            mPreview.setAspectRatio(mPreviewSize.getWidth(), mPreviewSize.getHeight());

            mCameraManager.openCamera(mCameraId, mCameraDeviceCallback, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    // set mCameraID first before openCamera()
    private boolean chooseBackFacingCamera() {
        try {
            final String[] ids = mCameraManager.getCameraIdList();
            if (ids.length == 0) {
                throw new RuntimeException("No camera available");
            }

            for (String id: ids) {
                CameraCharacteristics characteristics = mCameraManager.getCameraCharacteristics(id);
                Integer internal = characteristics.get(CameraCharacteristics.LENS_FACING);
                if (internal == null) {
                    throw new NullPointerException("Unexpected state: LENS_FACING null");
                }
                if (internal == FACING_BACK) {
                    mCameraId = id;
                    mCameraCharacteristics = characteristics;
                    return true;
                }
            }
        } catch (CameraAccessException e) {
            throw new RuntimeException("Failed to get a list of camera devices", e);
        }

        return false;
    }

    private Size chooseVideoSize(Size[] sizes) {
        for (Size size: sizes) {
            if (size.getWidth() == (size.getHeight() * 4 / 3) && size.getHeight() <= 1080) {
                Log.d(TAG, "chooseVideoSize: " + String.valueOf(size));
                return size;
            }
        }
        Log.e(TAG, "Could not find any suitable video size");
        return sizes[sizes.length - 1];
    }

    private Size choosePreviewSize(Size[] sizes, int textureViewWidth, int textureViewHeight, Size aspectRatio) {

        // Collect the supported resolutions that are at least as big as the preview Surface
        List<Size> bigEnough = new ArrayList<>();
        // Collect the supported resolutions that are smaller than the preview Surface
        List<Size> notBigEnough = new ArrayList<>();
        int w = aspectRatio.getWidth();
        int h = aspectRatio.getHeight();
        for (Size option : sizes) {
            if (option.getWidth() <= 1920 && option.getHeight() <= 1080 &&
                    option.getHeight() == option.getWidth() * h / w) {
                if (option.getWidth() >= textureViewWidth &&
                        option.getHeight() >= textureViewHeight) {
                    bigEnough.add(option);
                } else {
                    notBigEnough.add(option);
                }
            }
        }

        // Pick the smallest of those big enough. If there is no one big enough, pick the
        // largest of those not big enough.
        if (bigEnough.size() > 0) {
            return Collections.min(bigEnough, new CompareSizesByArea());
        } else if (notBigEnough.size() > 0) {
            return Collections.max(notBigEnough, new CompareSizesByArea());
        } else {
            Log.e(TAG, "Couldn't find any suitable preview size");
            return sizes[0];
        }
    }

    private void startPreviewSession() {
        // Check if SurfaceTexture is ready
        if (null == mCamera || !mPreview.isReady()) {
            return;
        }

        Log.d(TAG, "startPreviewSession");
        try {
            stopCaptureSession();

            mPreviewBuilder = mCamera.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);

            SurfaceTexture texture = mPreview.getSurfaceTexture();
            texture.setDefaultBufferSize(mPreviewSize.getWidth(),
                    mPreviewSize.getHeight());

            Surface previewSurface = new Surface(texture); // Error: TextureView.SurfaceTextureListener not implemented yet
            mPreviewBuilder.addTarget(previewSurface);

            mCamera.createCaptureSession(Collections.singletonList(previewSurface),
                    mSessionCallback, null);
        } catch (CameraAccessException e) {
            Log.e(TAG, "startPreviewSession: Error creating preview session " + e);
        }
    }

    public boolean startRecording() {
        if (mCamera == null) {
            return false;
        }

        try {
            stopCaptureSession();

            List<Surface> surfaces = new ArrayList<>();

            mPreviewBuilder = mCamera.createCaptureRequest(CameraDevice.TEMPLATE_RECORD);

            mPreviewBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
            mPreviewBuilder.set(CaptureRequest.CONTROL_SCENE_MODE, CameraMetadata.CONTROL_SCENE_MODE_DISABLED);
            mPreviewBuilder.set(CaptureRequest.CONTROL_AF_MODE, CameraMetadata.CONTROL_AF_MODE_OFF);
            mPreviewBuilder.set(CaptureRequest.LENS_FOCUS_DISTANCE, 0.0f);
            mPreviewBuilder.set(CaptureRequest.CONTROL_AE_MODE, CameraMetadata.CONTROL_AE_MODE_ON);
            // TODO Check for a list of available FPS
            mPreviewBuilder.set(CaptureRequest.CONTROL_AE_TARGET_FPS_RANGE, new Range<Integer>(30, 30));
            mPreviewBuilder.set(CaptureRequest.CONTROL_AWB_MODE, CameraMetadata.CONTROL_AWB_MODE_AUTO);
            mPreviewBuilder.set(CaptureRequest.CONTROL_VIDEO_STABILIZATION_MODE, CameraMetadata.CONTROL_VIDEO_STABILIZATION_MODE_OFF);
            mPreviewBuilder.set(CaptureRequest.LENS_OPTICAL_STABILIZATION_MODE, CameraMetadata.LENS_OPTICAL_STABILIZATION_MODE_OFF);

            // Preview Surface
            SurfaceTexture texture = mPreview.getSurfaceTexture();
            texture.setDefaultBufferSize(mPreviewSize.getWidth(),
                    mPreviewSize.getHeight());
            Surface previewSurface = new Surface(texture);
            surfaces.add(previewSurface);
            mPreviewBuilder.addTarget(previewSurface);

            // Encoder Surface
            if (!mEncoder.isReady()) {
                Log.d(TAG, "reconfigure");
                mEncoder.configure();
            }
            Surface encoderSurface = mEncoder.getInputSurface();
            surfaces.add(encoderSurface);
            mPreviewBuilder.addTarget(encoderSurface);

            mCamera.createCaptureSession(surfaces, new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(@NonNull CameraCaptureSession session) {
                    mCameraCaptureSession = session;
                    updateRecording();

                    Log.d(TAG, "Recording Session Configured Success");
                }

                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession session) {
                    Log.e(TAG, "Failed to configure recording session");
                }


            }, null);

            // updateTimer
            mStartTime = System.currentTimeMillis();
            mTimerHandler.postDelayed(updateTimerThread, 0);

            // Start writing data
            mDataWriter.startWriting(MDDataDirectory.getDataDir(mContext));
            // Write InitData
            mDataWriter.writeEvent(getInitData());

            mSensorLogger = new MDSensorLogger(mContext, mDataWriter);
            mSensorLogger.startLoggin();

            mRotateTimer.schedule(mRotateTask, 60000L, 60000L);
            mIsRecording = true;
            distanceTravel = 0;
            Log.d(TAG, "startRecording");
            return true;
        } catch (CameraAccessException e) {
            e.printStackTrace();
            return false;
        }
    }

    public void stopRecording() {
        startPreviewSession();

        // stopTimerThread
        mTimerHandler.removeCallbacks(updateTimerThread);
        mRotateTask.cancel();
        mRotateTimer.cancel();

        mSensorLogger.stopLogging();
        mSensorLogger = null;

        mEncoder.end();

        mIsRecording = false;
        Log.d(TAG, "stopRecording");
    }

    public View getView() {
        return mPreview.getView();
    }

    private void updatePreview() {
        try {
            mCameraCaptureSession.setRepeatingRequest(mPreviewBuilder.build(),
                    null, null);
        } catch (CameraAccessException e) {
            Log.e(TAG, "updatePreview: Failed to update camera preview", e);
        }
    }

    private void updateRecording() {
        try {
            mCameraCaptureSession.setRepeatingRequest(mPreviewBuilder.build(),
                    mCaptureCallback, null);
        } catch (CameraAccessException e) {
            Log.e(TAG, "updatePreview: Failed to update camera preview", e);
        }
    }

    private void stopCaptureSession() {
        if (mCameraCaptureSession != null) {
            mCameraCaptureSession.close();
            mCameraCaptureSession = null;
        }
    }

    private final Handler mTimerHandler = new Handler();

    private long mStartTime = 0;

    private Runnable updateTimerThread = new Runnable() {
        @Override
        public void run() {
            long millis = System.currentTimeMillis() - mStartTime;
            int secs = (int) (millis / 1000);
            int mins = secs / 60;
            secs = secs % 60;

            mCallback.onRecording(secs, mins);

            mTimerHandler.postDelayed(this, 1000);
        }
    };

    static class CompareSizesByArea implements Comparator<Size> {
        @Override
        public int compare(Size lhs, Size rhs) {
            // We cast here to ensure the multiplications won't overflow
            return Long.signum((long) lhs.getWidth() * lhs.getHeight() -
                    (long) rhs.getWidth() * rhs.getHeight());
        }

    }

    private Event getInitData() {
        Event event = new Event();
        Logger.Event.Builder builder = event.builder;

        Logger.InitData.Builder initData = builder.initInitData();
        initData.setDeviceType(Logger.InitData.DeviceType.ANDROID);
        initData.setVersion("0.1");

        Logger.InitData.AndroidBuildInfo.Builder buildInfo = initData.initAndroidBuildInfo();
        buildInfo.setBoard(Build.BOARD);
        buildInfo.setBootloader(Build.BOOTLOADER);
        buildInfo.setBrand(Build.BRAND);
        buildInfo.setDevice(Build.DEVICE);
        buildInfo.setDisplay(Build.DISPLAY);
        buildInfo.setFingerprint(Build.FINGERPRINT);
        buildInfo.setHardware(Build.HARDWARE);
        buildInfo.setId(Build.ID);
        buildInfo.setManufacturer(Build.MANUFACTURER);
        buildInfo.setModel(Build.MODEL);
        buildInfo.setModel(Build.PRODUCT);
        buildInfo.setRadioVersion(Build.getRadioVersion());
        buildInfo.setSerial(Build.SERIAL);
        buildInfo.initSupportedAbis(Build.SUPPORTED_ABIS.length);
        int i = 0;
        for (String s: Build.SUPPORTED_ABIS) {
            buildInfo.getSupportedAbis().set(i, new Text.Reader(s));
            i++;
        }
        buildInfo.setTags(Build.TAGS);
        buildInfo.setTime(Build.TIME);
        buildInfo.setType(Build.TYPE);
        buildInfo.setUser(Build.USER);
        buildInfo.setVersionCodename(Build.VERSION.CODENAME);
        buildInfo.setVersionRelease(Build.VERSION.RELEASE);
        buildInfo.setVersionSdk(Build.VERSION.SDK_INT);
        buildInfo.setVersionSecurityPatch(Build.VERSION.SECURITY_PATCH);

        return event;
    }

    public class RecordingTask extends TimerTask {
        @Override
        public void run() {
            mDataWriter.rotate();
        }
    }
}
