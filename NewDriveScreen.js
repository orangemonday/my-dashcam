import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  PermissionsAndroid,
  Platform,
  StatusBar,
  Dimensions,
  TouchableOpacity,
  Button,
} from "react-native";

import Orientation from 'react-native-orientation';
import MDCamera from './MDCamera';

async function requestCameraPermission() {
  if (Platform.OS === 'android') {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.CAMERA,
      {
        'title': 'Permission',
        'message': 'MyDashcam needs access to your camera to capture your trip',
      }
    );

    return granted === PermissionsAndroid.RESULTS.GRANTED
  }

  return false;
}

async function requestPermission() {
  if (Platform.OS === 'android') {
    const permissions = [
      PermissionsAndroid.PERMISSIONS.CAMERA,
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
    ];

    const granted = await PermissionsAndroid.requestMultiple(
      permissions
    );
    
    for (const p of permissions) {
      if (granted[p] !== PermissionsAndroid.RESULTS.GRANTED) {
        return false;
      }
    }
    return true;
  }

  return false;
}

class NewDriveScreen extends Component {
  static navigationOptions = {
    header: null,
  }

  constructor() {
    super()
    this.state = {
      isAuthorized: false,
      isRecording: false,
      secs: "00",
      mins: "00",
      distanceTravel: 0,
      speed: 0,
      isLandscape: true,
    };
  }

  async componentWillMount() {
    const isAuthorized = await requestPermission();
    this.setState({isAuthorized});

    Orientation.addOrientationListener(this._orientationDidChange);
    Orientation.lockToLandscape();
  }

  componentDidMount() {
    // remove status bar
    StatusBar.setHidden(true);

    Orientation.getOrientation((err, orientation) => {
      this.setState({isLandscape: (orientation === 'LANDSCAPE')});
    });
  }

  componentWillUnmount() {
    Orientation.unlockAllOrientations();
    Orientation.removeOrientationListener(this._orientationDidChange);
  }

  _orientationDidChange = (orientation) => {
    this.setState({isLandscape: (orientation === 'LANDSCAPE')});
  }

  async startRecording() {
    if (this.camera) {
      const isRecording = await this.camera.startRecording();
      this.setState({isRecording});
    }
  }

  stopRecording() {
    if (this.camera) {
      this.camera.stopRecording();
      this.setState({
        isRecording: false,
        secs: "00",
        mins: "00",
        speed: 0,
        distanceTravel: 0
      });
    }
  }

  onRecordingMessage(message) {
    const secs = ("0" + message.secs).slice(-2);
    const mins = ("0" + message.mins).slice(-2);
    this.setState({secs, mins});
  }

  onLocationUpdateMessage(message) {
    var distanceTravel = message.distanceTravel / 1000;
    distanceTravel = Math.round(distanceTravel * 10) / 10;
    // m/s to km/h = * 3600 / 1000
    var speed = Math.round(message.speed * 3.6);
    this.setState({distanceTravel, speed});
  }

  _renderOverlay() {
    if (this.state.isRecording) {
      return (
        <View style={styles.overlay}>
          <TouchableOpacity
            onPress={this.stopRecording.bind(this)}
            style={styles.recordButton}
          >
            <View style={styles.stopIcon}>
              <View style={styles.stopIconInner} />
            </View>
            <View style={styles.recordDesc}>
              <Text style={[styles.whiteText, styles.timerText]}>{this.state.mins}:{this.state.secs}</Text>
              <Text style={[styles.whiteText, styles.cameraStatusText]}>Recording</Text>
            </View>
          </TouchableOpacity>

          <View style={styles.speedometer}>
            <Text style={[styles.whiteText, styles.valueText]}>{this.state.speed}</Text>
            <Text style={[styles.whiteText, styles.unitText]}>kph</Text>
          </View>

          <View style={styles.driveStat}>
            <View style={styles.driveStatCol}>
              <Text style={[styles.whiteText, styles.valueText]}>{this.state.distanceTravel}</Text>
              <Text style={[styles.whiteText, styles.unitText]}>km</Text>
            </View>
            <View style={styles.driveStatCol}>
              <Text style={[styles.whiteText, styles.valueText]}>{this.state.mins}</Text>
              <Text style={[styles.whiteText, styles.unitText]}>points</Text>
            </View>
          </View>
        </View>
      )
    }
    return (
      <View style={styles.overlay}>
        <TouchableOpacity
          onPress={this.startRecording.bind(this)}
          style={styles.recordButton}
        >
          <View style={styles.readyIcon}>
            <View style={styles.readyIconInner} />
          </View>
          <View style={styles.recordDesc}>
            <Text style={[styles.whiteText, styles.timerText]}>00:00</Text>
            <Text style={[styles.whiteText, styles.cameraStatusText]}>Ready</Text>
          </View>
        </TouchableOpacity>
        
        <View style={styles.instruction}>
          <Text style={[styles.whiteText, styles.instructionTitle]}>Mount your phone and start driving</Text>
          <Text style={[styles.whiteText, styles.instructionSubtitle]}>Recording will begin automatically at 20 kph</Text>
        </View>
        <Button
          onPress={() => {this.props.navigation.navigate('Home')}}
          title='Go back'
        />
      </View>
    )
  }

  render() {
    if (this.state.isAuthorized && this.state.isLandscape) {
      return (
        <View style={styles.container}>
          <MDCamera
            style={styles.camera}
            ref={ref => {
              this.camera = ref;
            }}
            onRecordingMessage={this.onRecordingMessage.bind(this)}
            onLocationUpdateMessage={this.onLocationUpdateMessage.bind(this)}
          />
          
          {this._renderOverlay()}
        </View>
      )
    } else {
      return (
        <View style={styles.container}>
        </View>
      )
    }
 }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
  },
  camera: {
    flex: 1,
  },
  overlay: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
  },
  recordButton: {
    position: 'absolute',
    top: 32,
    width: 132,
    height: 60,
    borderRadius: 30,
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  speedometer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    margin: 20,
    height: 80,
    width: 80,
    borderRadius: 80,
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  driveStat: {
    position: 'absolute',
    top: 0,
    right: 0,
    margin: 32,
    height: 48,
    width: 120,
    borderRadius: 30,
    backgroundColor: 'rgba(0, 0, 0, 0.6)',

    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  driveStatCol: {
    marginLeft: 8,
    marginRight: 8,
    alignItems: 'center',
  },
  valueText: {
    fontSize: 18,
  },
  unitText: {
    fontSize: 12,
  },
  stopIcon: {
    height: 32,
    width: 32,
    borderRadius: 32,
    borderWidth: 2,
    borderColor: 'white',
    backgroundColor: 'rgba(0, 0, 0, 0)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  stopIconInner: {
    height: 12,
    width: 12,
    borderRadius: 2,
    backgroundColor: 'red',
  },
  readyIcon: {
    height: 32,
    width: 32,
    borderRadius: 32,
    backgroundColor: 'rgba(255, 255, 255, 0.6)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  readyIconInner: {
    height: 28,
    width: 28,
    borderRadius: 28,
    backgroundColor: 'white',
  },
  recordDesc: {
    alignItems: 'center',
    margin: 8,
  },
  instruction: {
    width: '50%',
    margin: 20,
  },
  cameraStatusText: {
    fontSize: 10,
  },
  timerText: {
    fontWeight: 'bold',
    fontSize: 18,
  },
  instructionTitle: {
    padding: 4,
    fontWeight: 'bold',
    fontSize: 18,
    textAlign: 'center',
  },
  instructionSubtitle: {
    padding: 4,
    fontSize: 12,
    textAlign: 'center',
  },
  whiteText: {
    color: 'white',
  },
})

export default NewDriveScreen;

