import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  TouchableOpacity,
  SectionList,
  RefreshControl,
  Image,
  NativeModules,
  Button,
} from "react-native";

const MDModule = NativeModules.MDModule;

class MainScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'MyDashcam',
      headerRight: (
        <View style={{marginRight: 16}}>
          <Button
            onPress={() => navigation.navigate("Setting")}
            title="Settings"
            color="#000"
          />
        </View>
      )
    }
  };

  constructor(props) {
    super(props)
    this.state = {
      refreshing: false,
      overviewData: [
        { title: "This week", numOfDrive: 1 },
        { title: "This month", numOfDrive: 28 },
        { title: "This year", numOfDrive: 1127 },
        { title: "All time", numOfDrive: 3380 },
      ],
      data: [
        { 
          src: './drive.png',
          place: 'Kuala Lumpur (from Kuala Lumpur)',
          date: '6/11/2018',
          time: '1:00 PM - 1:16 PM',
        },
        { 
          src: './drive.png',
          place: 'Kuala Lumpur (from Kuala Lumpur)',
          date: '6/11/2018',
          time: '1:00 PM - 1:16 PM',
        },
        { 
          src: './drive.png',
          place: 'Kuala Lumpur (from Kuala Lumpur)',
          date: '6/11/2018',
          time: '1:00 PM - 1:16 PM',
        },
        { 
          src: './drive.png',
          place: 'Kuala Lumpur (from Kuala Lumpur)',
          date: '6/11/2018',
          time: '1:00 PM - 1:16 PM',
        },
      ]
    }
  }

  _renderGroupDriveItem({item, index, section: {title}}) {
    // TODO: pass image source
    return (
      <View style={styles.listItem}>
        <Text style={{fontWeight: 'bold'}}>{item.title}</Text>
        <Text>{item.numOfDrive} drives recorded</Text>
      </View>
    )
  }

  _renderDriveItem = ({item}) => {
    return (
      <TouchableOpacity
        onPress={() => this.props.navigation.navigate("DriveDetail")}>
        <View style={[styles.listItem, styles.driveItem]}>
          <Image style={styles.drivePreview} source={require('./drive.png')} />
          <View style={styles.driveItemDesc}>
            <Text>{item.place}</Text>
            <Text>{item.date}, {item.time}</Text>
          </View>
        </View>
      </TouchableOpacity>
    )
  }

  _renderSectionHeader({section: {title}}) {
    if (title === 'null') {
      return(
        <View />
      )
    }
    return(
      <View style={styles.listHeader}>
        <Text>{title}</Text>
      </View>
    )
  };

  _onRefresh() {
    this.setState({refreshing: true});
    setInterval(() => {
      this.setState({refreshing: false});
    }, 500);
  }

  _onPressIn() {
  }

  render() {
    return (

      <View style={styles.container}>
        <SectionList
          renderItem={(this._renderDriveItem)}
          renderSectionHeader={(this._renderSectionHeader)}
          sections={[
            {title: 'null', data: this.state.overviewData, renderItem: this._renderGroupDriveItem, renderSectionHeader: this._renderEmptyHeader},
            {title: 'June, 2018', data: this.state.data},
          ]}
          keyExtractor={(item, index) => item + index}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh.bind(this)}
            />}
        />


        <View style={styles.tabbar}>
          <TouchableHighlight
            onPressIn={this._onPressIn}
            style={styles.tabItem}>
            <View>
              <Text style={styles.tabItemText}>Dashboard</Text>
            </View>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={() => this.props.navigation.navigate("NewDrive")}
            style={styles.tabItem}>
            <View>
              <Text style={styles.tabItemText}>New Drive</Text>
            </View>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={() => MDModule.startUploaderService()}
            style={styles.tabItem}>
            <View>
              <Text style={styles.tabItemText}>Upload</Text>
            </View>
          </TouchableHighlight>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  listHeader: {
    padding: 8,
    borderBottomWidth: 0.5,
    borderColor: "#bbbbbb",
  },
  listItem: {
    padding: 12,
    borderBottomWidth: 0.5,
    borderColor: "#bbbbbb",
  },
  driveItem: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  driveItemDesc: {
    marginLeft: 16,
  },
  drivePreview: {
    borderRadius: 25,
    height: 50,
    width: 50,
    resizeMode: 'contain',
  },
  tabbar: {
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    height: 64,
    
    flex: 1,
    flexDirection: "row",
    backgroundColor: "#cccccc",
  },
  tabItem: {
    flex: 1,
    borderLeftWidth: 0.5,
    borderRightWidth: 0.5,
    borderColor: "white",
    backgroundColor: "darkblue",
    alignItems: "center", 
    justifyContent: "center",
  },
  tabItemText: {
    color: "white"
  },
})

export default MainScreen;

