package com.mydashcam.camera;

import android.util.Log;
import android.widget.FrameLayout;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.events.RCTEventEmitter;

public class MDCameraView extends FrameLayout implements LifecycleEventListener{

    private CameraViewImpl mImpl;

    private ThemedReactContext mThemedReactContext;

    private DisplayOrientationDetector mDisplayOrientationDetector;

    public MDCameraView(ThemedReactContext themedReactContext) {
        super(themedReactContext);

        mThemedReactContext = themedReactContext;
        themedReactContext.addLifecycleEventListener(this);

        final TextureViewPreview preview = new TextureViewPreview(themedReactContext, this);

        CameraViewImpl.Callback callbacks = new CameraViewImpl.Callback() {
            @Override
            public void onRecording(int secs, int mins) {
                dispatchOnRecording(secs, mins);
            }

            @Override
            public void onLocationUpdate(float distanceTravel, float speed) {
                dispatchOnLocationUpdate(distanceTravel, speed);
            }
        };

        mImpl = new MDCamera(callbacks, themedReactContext, preview);

        mDisplayOrientationDetector = new DisplayOrientationDetector(themedReactContext) {
            @Override
            public void onDisplayOrientationChanged(int displayOrientation) {
                preview.setDisplayOrientation(displayOrientation);
            }
        };
    }

    public void start() {
        mImpl.start();
    }

    public void stop() {
        mImpl.stop();
    }

    public void startRecording(final Promise promise) {
        if (mImpl.startRecording()) {
            promise.resolve(true);
        } else {
            promise.resolve(false);
        }
    }

    private void dispatchOnRecording (int secs, int mins) {
        WritableMap event = Arguments.createMap();
        event.putInt("secs", secs);
        event.putInt("mins", mins);
        ReactContext reactContext = (ReactContext)getContext();
        reactContext.getJSModule(RCTEventEmitter.class).receiveEvent(
                getId(),
                "recording",
                event
        );
        Log.d("DISPATCH", "dispatchOnRecording");
    }

    private void dispatchOnLocationUpdate(float distanceTravel, float speed) {
        WritableMap event = Arguments.createMap();
        event.putDouble("distanceTravel", distanceTravel);
        event.putDouble("speed", speed);
        ReactContext reactContext = (ReactContext)getContext();
        reactContext.getJSModule(RCTEventEmitter.class).receiveEvent(
                getId(),
                "locationupdate",
                event
        );
        Log.d("DISPATCH", "dispatchOnLocationUpdate");
    }

    public void stopRecording() {
        mImpl.stopRecording();
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        mDisplayOrientationDetector.enable(getDisplay());
    }

    @Override
    protected void onDetachedFromWindow() {
        mDisplayOrientationDetector.disable();
        super.onDetachedFromWindow();
    }

    @Override
    public void onHostResume() {
        // start();
    }

    @Override
    public void onHostPause() {
        stop();
    }

    @Override
    public void onHostDestroy() {
        stop();
        mThemedReactContext.removeLifecycleEventListener(this);
    }
}
